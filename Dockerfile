FROM node:12-alpine

WORKDIR app

COPY index.js package.json ./.npmrc ./

RUN yarn

ENTRYPOINT ["yarn", "start"]

EXPOSE 4000

ENV PORT=4000
ENV DATABASE_URL=postgresql://shuttle:shuttle@localhost:5432/shuttle
ENV MINIO_ACCESS_KEY=shuttle
ENV MINIO_SECRET_KEY=shuttleminio
ENV MINIO_ENDPOINT=localhost
ENV MINIO_PORT=9000
ENV MINIO_USE_SSL=false
ENV MINIO_DEFAULT_BUCKET=default
ENV SESSION_SECRET=keyboard-cat-dart
