[![Deploy on Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/lo-agency/apollo/shuttle-production/tree/master)

# Setup using docker
1. `heroku container:login`
2. `heroku container:push web -a lo-website-admin`
2. `heroku container:release web -a lo-website-admin`
